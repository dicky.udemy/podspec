#
# Be sure to run `pod lib lint ConversaOmnichannel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ConversaOmnichannel'
  s.version          = '0.1.0'
  s.summary          = 'This app is for Omnichannel app for iOS made by Prosa'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'Omnichannel Conversa is aplication SDK for development about Omnichannel app by Prosa.'

  s.homepage         = "https://gitlab.com/dicky.udemy/ConversaOmnichannel.git"
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'udemy-freelance' => 'dicky.udemy@gmail.com' }
  s.source           = { :git => "https://gitlab.com/dicky.udemy/ConversaOmnichannel.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'

  s.source_files = 'ConversaOmnichannel/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ConversaOmnichannel' => ['ConversaOmnichannel/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
